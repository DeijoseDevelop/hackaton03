from celery import shared_task
from django.utils.crypto import get_random_string
from .forms import ProductForm, RegisterForm
from .models import *


def import_users(user, instance):
    obj = instance
    password = get_random_string(length=32)
    # User
    obj.username = user['username']
    obj.first_name = user['first_name']
    obj.last_name = user['last_name']
    obj.phone = user['phone_number']
    obj.email = user['email']
    obj.address = user['address']
    obj.password1 = password
    obj.password2 = password
    print(obj.username)
    data_user = {
        'username': obj.username,
        'first_name': obj.first_name,
        'last_name': obj.last_name,
        'phone': obj.phone,
        'email': obj.email,
        'address': obj.address,
        'password1': obj.password1,
        'password2': obj.password2,
    }
    form_user = RegisterForm(data_user)
    if form_user.is_valid():
        form_user.save()
    return obj

def import_products(product, instance):
    obj = instance

    # Product
    obj.name = product['name_product']
    obj.price = product['price_product']
    obj.amount_product = product['amount_product']

    data_product = {
        'name': obj.name,
        'price': obj.price,
        'amount_product': obj.amount_product
    }
    form_product = ProductForm(data_product)
    if form_product.is_valid():
        form_product.save()
    return obj

def import_shopping(shopping, instance_shopping, instance_user, instance_product):
    obj = instance_shopping
    obj.client_id = instance_user
    obj.product_id = instance_product

    print(obj.client_id)
    print(obj.product_id)

    data_shopping = {
        'client_id': obj.client_id,
        'product_id': obj.product_id,
    }
    form_shopping = ShoppingForm(data_shopping)
    if form_shopping.is_valid():
        form_shopping.save()

@shared_task
def import_data_excel(dicc):
    for item in dicc:

        password = get_random_string(length=32)
        product = Product()
        user = User()

        #Product
        product.name = item['name_product']
        product.price = item['price_product']
        product.amount_product = item['amount_product']

        #User
        user.username = item['username']
        user.first_name = item['first_name']
        user.last_name = item['last_name']
        user.phone = item['phone_number']
        user.email = item['email']
        user.address = item['address']
        user.password1 = password
        user.password2 = password
        data_product = {
            'name' : product.name,
            'price' : product.price,
            'amount_product' : product.amount_product
        }

        data_user = {
            'username' : user.username,
            'first_name' : user.first_name,
            'last_name' : user.last_name,
            'phone' : user.phone,
            'email' : user.email,
            'address' : user.address,
            'password1' : user.password1,
            'password2': user.password2,
        }

        #Forms
        form_user = RegisterForm(data_user)
        if form_user.is_valid():
            form_user.save()

        form_product = ProductForm(data_product)
        if form_product.is_valid():
            form_product.save()

        #Dataset
        instance_user = User()
        user = import_users(item, instance_user)
        instance_user = user
        instance_product = Product()
        product = import_products(item, instance_product)
        instance_product = product
        instance_shopping = Shopping()
        import_shopping(item, instance_shopping, instance_user, instance_product)
