from django.test import TestCase
from importer.models import User, Product


class ImporterTest(TestCase):

    def test_price(self):
        product = Product.objects.create(
            price=1.0, name='tests', amount_product=1
        )
        self.assertTrue(float(product.price))

    def test_address_user(self):
        user = User.objects.create_user(
            username='tests', password='tests', pay_method_id='cash', address='10490 Pennsylvania Court')
        self.assertEqual(len(user.address.split()), 3)

    def test_phone_user(self):
        user = User.objects.create_user(
            username='tests', password='tests', pay_method_id='cash', phone='(684) 8301806')
        phone = user.phone.split()
        phone_one = ''
        for i in phone[0]:
            if i.isnumeric():
                phone_one += i
        self.assertEqual(len(phone_one), 3)
        self.assertEqual(len(phone[1]), 7)
