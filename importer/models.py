from django.db import models
from django.contrib.auth.models import AbstractUser

METHODS = [
    ('Cash', 'cash'),
    ('Debit card', 'debit_card'),
    ('Paypal', 'paypal')
]


class User(AbstractUser):
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=20)
    pay_method_id = models.CharField(max_length=100, choices=METHODS)


class Product(models.Model):
    name = models.CharField(max_length=100)
    price = models.FloatField()
    amount_product = models.IntegerField()


class Shopping(models.Model):
    client_id = models.OneToOneField(User, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
