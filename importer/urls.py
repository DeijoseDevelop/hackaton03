from django.urls import path

from importer import views
from .views import LoginView, RegisterView, LogoutView, DetailUser

app_name = 'auth'

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('detail_user/<int:pk>/', DetailUser.as_view(), name='detail_user'),
    path('import/', views.import_product, name='import'),
]