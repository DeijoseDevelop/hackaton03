from django.shortcuts import render
from .models import User, Product, Shopping
from django.views.generic import FormView, CreateView
from django.views.generic import RedirectView, DetailView
from .forms import LoginForm, RegisterForm
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from .tasks import import_data_excel
from .utils.get_data import get_data


def import_product(request):
    if request.method == 'POST':
        filename = request.FILES['myfile']
        v = get_data(filename)
        import_data_excel(v)
    return render(request, 'importer/products.html')



class LoginView(FormView):
    """
    view to log in an already registered user
    """
    form_class = LoginForm
    template_name = 'auth/login.html'
    success_url = reverse_lazy('auth:detail_user')

    def form_valid(self, form):
        """
        function to modify the behavior of the form and to be able to save the data
        """
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        """
        function to modify the behavior of the redirection to redirect to each user
        """
        return reverse_lazy('auth:detail_user', kwargs={'pk': self.request.user.pk})

class DetailUser(LoginRequiredMixin, DetailView):
    """
    view to display all data for each user
    """
    model = User
    template_name = 'auth/detail_user.html'

    def get_context_data(self, **kwargs):
        """
        function to get the user and send it by context to the template
        """
        context = super(DetailUser, self).get_context_data(**kwargs)
        user = self.request.user
        #shopping = Shopping.objects.filter(client_id__client_id=user)
        shopping = Shopping.objects.get(id=1).user.all()
        print(shopping)
        context['user'] = user
        return context

class LogoutView( LoginRequiredMixin, RedirectView):
    """
    view to logout a user
    """
    url = reverse_lazy('auth:login')

    def get(self, request, *args, **kwargs):
        """
        function for unlogging a user
        """
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)

class RegisterView(CreateView):
    """
    view to register a new user
    """
    form_class = RegisterForm
    model = User
    template_name = 'auth/register.html'
    success_url = reverse_lazy('auth:login')

    def form_valid(self, form):
        """
        function to save and register user
        """
        form.save()
        return super(RegisterView, self).form_valid(form)
