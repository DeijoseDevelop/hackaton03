import pandas as pd

def get_data(filename):
    df_list = []
    df = pd.read_excel(filename)
    df.fillna('', inplace=True)
    columns_dict = [x for x in df.columns]
    for element in df.index.values:
        df_line = df.loc[element, columns_dict].to_dict()
        df_list.append(df_line)

    return df_list

