from django import forms
from .models import *
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm


class LoginForm(AuthenticationForm):
    username = forms.CharField(label='Username',
                               max_length=30,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control',
                                          'placeholder': 'Enter Your username'}))
    password = forms.CharField(label='Password',
                               max_length=30,
                               widget=forms.PasswordInput(
                                   attrs={'class': 'form-control',
                                          'placeholder': 'Enter Your password'}))


class RegisterForm(UserCreationForm):
    username = forms.CharField(label='Username',
                               max_length=30,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control',
                                          'placeholder': 'Enter Your username'}))
    email = forms.EmailField(label='Email',
                             widget=forms.EmailInput(
                                 attrs={'class': 'form-control',
                                        'placeholder': 'Enter your email'}))
    first_name = forms.CharField(label='First Name',
                                 max_length=30,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control',
                                            'placeholder': 'Enter your first name'}))
    last_name = forms.CharField(label='Last Name',
                                max_length=30,
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control',
                                           'placeholder': 'Enter your last name'}))
    phone = forms.CharField(label='Phone',
                            max_length=20,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control',
                                        'placeholder': 'Enter your phone'}))
    address = forms.CharField(label='Address',
                              widget=forms.Textarea(
                                    attrs={'class': 'form-control',
                                             'placeholder': 'Enter your address',
                                             'rows': '3', 'cols': '5'}))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'phone', 'email', 'address')

class ProductForm(forms.ModelForm):
    name = forms.CharField(label='Name', max_length=30)
    price = forms.FloatField(label='Price')
    amount_product = forms.IntegerField(label='Amount')

    class Meta:
        model = Product
        fields = ('name', 'price', 'amount_product')

class ShoppingForm(forms.ModelForm):
    client_id = forms.ModelChoiceField(label='Client', queryset=User.objects.all())
    product_id = forms.ModelChoiceField(label='Product', queryset=Product.objects.all())
    class Meta:
        model = Shopping
        fields = ('client_id', 'product_id')